'use strict';

module.exports = {
    get EventEmitter () {
        return require('./lib/EventEmitter');
    },

    get Express () {
        return require('./lib/Express');
    },

    get Timer () {
        return require('./lib/Timer')
    },

    get Timing () {
        return require('./lib/Timing');
    }
};
