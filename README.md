# node-server-timing

Record server timings and respond via a Server Timing header.

# Express Example

    const { Express : ExpressTiming } = require('../index');

    const express = require('express');
    const app     = express();

    app.use(ExpressTiming.init());

    app.get('/', (req, res) => {
        req.startTiming('foo', 'Doing the foo');
        req.startTiming('bar', 'Barring the bar');
        req.startTiming('baz', 'Just bazin');

        setTimeout(() => {
            res.stopTiming('bar');
        }, 100);

        setTimeout(() => {
            res.stopTiming('baz');
        }, 300);

        setTimeout(() => {
            res.stopTiming('foo');

            res.send('Hello World!');
        }, 500);
    });

    app.listen(3000, () => {
        console.log('Example app listening on port 3000!')
    });


The express middleware adds the `startTiming` and `stopTiming` methods onto both the `req` and `res` so they can be used interchangeably.

The `startTiming` method accepts two arguments:
- `key` The key of the timing. This can be used when you stop the timer.
- `description` A short description of the timer. This defaults to the `key` if not provided.

The `startTiming` method also returns the timer with a handy `stop` and `clear` method on it:

    const timer = req.startTiming('foo');

    // same as executing req.startTiming('foo');
    timer.stop();
    // removes the timer
    timer.clear();

The `stopTiming` method only accepts a single argument:
- `key` The key used to start a timer.
