'use strict';

const Timing = require('./Timing');

class Express {
    static init (config) {
        return function (req, res, next) {
            const timing   = Timing.decorate(config, req, res);
            const listener = () => {
                res.set('Server-Timing', timing.toString());
            }

            timing.on('stop', listener);

            res.once('finish', () => {
                timing.un('stop', listener);
            });

            next();
        }
    }
}

module.exports = Express;
