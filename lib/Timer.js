'use strict';

class Timer {
    constructor (key, desc = key) {
        this.desc = desc;
        this.key  = key;
    }

    start () {
        this.start = process.hrtime();

        return this;
    }

    stop () {
        const end = process.hrtime(this.start);

        this.end = end[0] * 1e9 + end[1] / 1000000;

        this.isStopped = true;

        return this;
    }

    toString () {
        return `${this.key}=${this.end.toFixed(2)}; "${this.desc}"`;
    }
}

module.exports = Timer;
