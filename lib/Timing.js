'use strict';

const EventEmitter = require('./EventEmitter');
const Timer        = require('./Timer');

class Timing extends EventEmitter {
    static decorate (config = {}, ...args) {
        const timing   = new this();
        const property = config.property || '$serverTiming';

        args.forEach(arg => {
            arg[property] = timing;

            arg.startTiming = timing.start.bind(timing);
            arg.stopTiming  = timing.stop.bind(timing);
        });

        return timing;
    }

    constructor () {
        super();

        this.map = new Map();
    }

    start (name, desc = name) {
        const me  = this;
        const map = me.map;

        if (map.get(name)) {
            throw new Error('Timer already set');
        } else {
            const timer = new Timer(name, desc).start();

            map.set(name, timer);

            me.emit('start', timer);

            timer.clear = () => {
                timer.stop();

                me.clear(name);

                return me;
            };

            return timer;
        }
    }

    stop (name) {
        const timer = this.map.get(name);

        if (timer) {
            timer.stop();

            this.emit('stop', timer);
        }
    }

    clear (name) {
        this.map.delete(name);
    }

    toString () {
        const header = [];

        this.map.forEach(timer => {
            if (timer.isStopped) {
                header.push(timer.toString());
            }
        });

        return header.join();
    }
}

module.exports = Timing;
