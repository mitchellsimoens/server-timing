const { Express : ExpressTiming } = require('../index');

const express = require('express');
const app     = express();

app.use(ExpressTiming.init());

app.get('/', (req, res) => {
    req.startTiming('foo', 'Doing the foo');
    req.startTiming('bar', 'Barring the bar');
    req.startTiming('baz', 'Just bazin');

    setTimeout(() => {
        res.stopTiming('bar');
    }, 100);

    setTimeout(() => {
        res.stopTiming('baz');
    }, 300);

    setTimeout(() => {
        res.stopTiming('foo');

        res.send('Hello World!');
    }, 500);
});

app.listen(3000, () => {
    console.log('Example app listening on port 3000!')
});
